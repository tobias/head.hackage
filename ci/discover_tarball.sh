#! /usr/bin/env bash

set -e

cd "$(dirname "${BASH_SOURCE[0]}")"

>&2 echo "NIGHTLY=${NIGHTLY}"
>&2 echo "RELEASE_JOB=${RELEASE_JOB}"

if [ -n "$NIGHTLY" ]; then
  JOB_NAME="nightly-x86_64-linux-fedora33-release"
elif [ "$RELEASE_JOB" == "yes" ]; then
  JOB_NAME="release-x86_64-linux-fedora33-release"
else
  JOB_NAME="x86_64-linux-fedora33-release"
fi

>&2 echo "JOB_NAME=${JOB_NAME}"

if [ -n "$UPSTREAM_COMMIT_SHA" ]; then
  # N.B. We can't use this if the upstream pipeline might be in-progress
  # since the below URL cannot provide an artifact until a pipeline has
  # run to completion on the requested branch. This is in general
  # not the case for GHC pipelines. Consequently, in this case we will
  # usually rather provide UPSTREAM_PIPELINE_ID.
  >&2 echo "Pulling binary distribution from commit $UPSTREAM_COMMIT_SHA of project $UPSTREAM_PROJECT_PATH..."
  GHC_TARBALL="https://gitlab.haskell.org/$UPSTREAM_PROJECT_PATH/-/jobs/artifacts/$UPSTREAM_COMMIT_SHA/raw/ghc-x86_64-linux-fedora33-release.tar.xz?job=$JOB_NAME"
elif [ -n "$UPSTREAM_PIPELINE_ID" ]; then
  job_name=$JOB_NAME
  >&2 echo "Pulling ${job_name} binary distribution from Pipeline $UPSTREAM_PIPELINE_ID..."
  job_id=$(find-job $UPSTREAM_PROJECT_ID $UPSTREAM_PIPELINE_ID $job_name)
  >&2 echo "Using job $job_id..."
  echo "https://gitlab.haskell.org/$UPSTREAM_PROJECT_PATH/-/jobs/$job_id/artifacts/raw/ghc-x86_64-linux-fedora33-release.tar.xz"
fi
