# vi: set filetype=sh

# Packages expected not to build due to GHC bugs. This is `source`'d by the CI
# script and the arguments in BROKEN_ARGS are added to the hackage-ci
# command-line.

# Mark the named package as broken.
#
# Usage:
#    broken $pkg_name $ghc_ticket_number
#
function broken() {
  pkg_name="$1"
  ticket="$2"
  echo "Marking $pkg_name as broken due to #$ticket"
  EXTRA_OPTS="$EXTRA_OPTS --expect-broken=$pkg_name"
}

function only_package() {
  echo "Adding $@ to --only package list"
  for pkg in $@; do
    EXTRA_OPTS="$EXTRA_OPTS --only=$pkg"
  done
}

function test_package() {
  echo "Adding $@ to --test-package list"
  EXTRA_OPTS="$EXTRA_OPTS --test-package=$1=$2"
}

# Return the version number of the most recent release of the given package
function latest_version() {
  pkg=$1
  curl -s -H "Accept: application/json" -L -X GET http://hackage.haskell.org/package/$pkg/preferred | jq '.["normal-version"] | .[0]' -r
}

# Add a package to the set of packages that lack patches but are nevertheless
# tested.
function extra_package() {
  pkg_name="$1"
  version="$2"
  if [ -z "$version" ]; then
    version=$(latest_version $pkg_name)
  fi
  echo "Adding $pkg_name-$version to extra package set"
  EXTRA_OPTS="$EXTRA_OPTS --extra-package=$pkg_name==$version"
}

# Mark a package to be declared with build-tool-depends, not build-depends.
# This is necessary for packages that do not have a library component.
function build_tool_package() {
  pkg_name="$1"
  echo "Adding $pkg_name as a build-tool package"
  EXTRA_OPTS="$EXTRA_OPTS --build-tool-package=$pkg_name"
}

if [ -z "$GHC" ]; then GHC=ghc; fi

function ghc_version() {
  $GHC --version | sed 's/.*version \([0-9]*\.\([0-9]*\.\)*\)/\1/'
}

function ghc_commit() {
  $GHC --print-project-git-commit-id
}

# ======================================================================
# The lists begin here
#
# For instance:
#
#    broken "lens" 17988

version="$(ghc_version)"
commit="$(ghc_commit)"
echo "Found GHC $version, commit $commit."
case $version in
  9.2.*)
    #       package                   ticket
    ;;

  9.4.*)
    #       package                   ticket
    ;;

  9.5.*)
    #       package                   ticket
    ;;

  *)
    echo "No broken packages for GHC $version"
    ;;
esac

# Extra packages
# ==============
#
# These are packages which we don't have patches for but want to test anyways.
extra_package lens
extra_package aeson
extra_package criterion
extra_package scotty
extra_package generic-lens
extra_package microstache
extra_package singletons-base 3.1
extra_package servant
extra_package hgmp
extra_package Agda
extra_package mmark
extra_package doctest 0.20.1
extra_package tasty
extra_package pandoc

# Build-tool packages
build_tool_package alex
build_tool_package happy

# $BUILD_MODE controls how head.hackage runs.
# ===========================================
#
# Four build modes exist: FULL, QUICK, TEST, and COMPAT.
#
# FULL.
# ------
# Build all patched + extra packages.
#
# QUICK.
# ------
# Build the "quick" configuration, which builds a small subset of the overall
# package set. (Also runs tests!) We do this during the merge request validation
# pipeline. Note: If "$QUICK" is non-null, it is used as a backwards-compat
# synonym for BUILD_MODE=QUICK.
#
# TEST.
# -----
# Just build the local test packages and run the tests.
#
# COMPAT: FULL + TEST.
# --------------------
# Backwards-compat default build mode.
#
: ${BUILD_MODE:=COMPAT}
if [ -n "$QUICK" ]; then
    BUILD_MODE=QUICK
fi
case "$BUILD_MODE" in
    FULL) ;;
    QUICK)
        only_package tasty
        only_package Cabal
        only_package microlens
        only_package free
        only_package optparse-applicative
        test_package system-test "$(pwd)/../tests/ghc-debug/test/"
        test_package ghc-tests "$(pwd)/../tests/ghc-tests"
        ;;
    TEST)
        # FIXME: I specify a single "only_package" to prevent all the other
        # packages from being built. Morally, I really want to say "build
        # nothing at all besides the tests".
        only_package tasty
        test_package system-test "$(pwd)/../tests/ghc-debug/test/"
        test_package ghc-tests "$(pwd)/../tests/ghc-tests"
        ;;
    COMPAT)
        test_package system-test "$(pwd)/../tests/ghc-debug/test/"
        test_package ghc-tests "$(pwd)/../tests/ghc-tests"
        ;;
esac
